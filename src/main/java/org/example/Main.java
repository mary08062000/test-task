package org.example;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Please specify the name of the text file as a command line argument");
            return;
        }
        String fileName = args[0];

        try (
                BufferedReader reader = Files.newBufferedReader(Paths.get(fileName));
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out))
        ) {
            String line;
            HashSet<Character> set = new HashSet<>();
            boolean firstChar = true;
            while ((line = reader.readLine()) != null) {
                for (int i = 0; i < line.length(); ++i) {
                    char character = line.charAt(i);
                    if (!Character.isWhitespace(character) && !set.contains(character)) {
                        set.add(character);
                        if (firstChar) {
                            writer.write(character);
                            firstChar = false;
                        } else {
                            writer.write(", " + character);
                        }
                    }
                }
            }
        } catch (NoSuchFileException e) {
            System.err.println("File not found " + fileName + ": " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Error reading file " + fileName + ": " + e.getMessage());
        }
    }
}
